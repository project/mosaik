<?php

/**
 * Pieces callback form: Block
 */
function pieces_callback_form_block(&$form, &$form_state) {
  $args = $form_state['piece']['callback_arguments'];
  $blocks = _pieces_get_blocks();

  $form['block'] = array(
    '#title' => t('Block'),
    '#type' => 'select',
    '#options' => $blocks,
    '#required' => TRUE,
    '#ajax' => array(
      'callback' => 'pieces_form_ajax_callback',
      'wrapper' => 'pieces-callback-wrapper',
    ),
    '#default_value' => !empty($args['module']) ? $args['module'] . ':' . $args['delta'] : NULL
  );

  if (!empty($form_state['values']['block']) && $form_state['triggering_element']['#name'] == 'block') {
    list($module, $delta) = explode(':', $form_state['values']['block']);

    $form_state['values']['callback_arguments'] = array(
      'module' => $module,
      'delta' => $delta,
    );
  }
}

/**
 * Helper function: returns a list of blocks.
 *
 * @return array
 */
function _pieces_get_blocks() {
  $blocks_list = &drupal_static(__FUNCTION__, array());

  if (empty($blocks_list)) {
    $blocks = _block_rehash();
    $blocks_list = array();

    foreach ($blocks as $block) {
      $key = $block['module'] . ':' . $block['delta'];
      $blocks_list[$key] = $block['info'] . ' (' . $block['module'] . ':' . $block['delta'] . ')';
    }
  }
  return $blocks_list;
}

/**
 * Pieces callback form: Entity
 */
function pieces_callback_form_entity(&$form, &$form_state) {
  $default = array(
    'entity_type' => '',
    'bundle' => '',
    'entity_id' => '',
    'entity_view_mode' => '',
  );
  $values = isset($form_state['values']) ? $form_state['values'] : array();
  $args = array_merge($default, $form_state['piece']['callback_arguments']);
  $entity_type = isset($values['entity_type']) ? $values['entity_type'] : $args['entity_type'];
  $bundle = isset($values['bundle']) ? $values['bundle'] : $args['bundle'];
  $entity_id = isset($values['entity_id']) ? $values['entity_id'] : $args['entity_id'];
  $view_mode = isset($values['entity_view_mode']) ? $values['entity_view_mode'] : $args['entity_view_mode'];
  list($entity_types, $bundles, $view_modes) = _pieces_entity_info();
  $autocomplete = module_exists('entity_autocomplete') && user_access('use entity autocomplete');
  $allowed_autocomplete = array(
    'node',
    'taxonomy_term',
  );

  if ($autocomplete) {
    // Allow other modules to alter allowed autocomplete entity types.
    drupal_alter('pieces_entity_allowed_autocomplete', $allowed_autocomplete);
  }

  $form['entity_type'] = array(
    '#title' => t('Entity type'),
    '#type' => 'select',
    '#required' => TRUE,
    '#options' => array('' => t('- Select -')) + $entity_types,
    '#ajax' => array(
      'callback' => 'pieces_form_ajax_callback',
      'wrapper' => 'pieces-callback-wrapper',
    ),
    '#default_value' => $entity_type,
  );

  if (!empty($entity_type)) {
    $form['bundle'] = array(
      '#title' => t('Bundle'),
      '#type' => 'select',
      '#required' => TRUE,
      '#options' => $bundles[$entity_type],
      '#ajax' => array(
        'callback' => 'pieces_form_ajax_callback',
        'wrapper' => 'pieces-callback-wrapper',
      ),
      '#default_value' => $bundle,
    );

    if (!empty($bundle)) {
      $form['entity_id'] = array(
        '#title' => t('ID'),
        '#type' => 'textfield',
        '#required' => TRUE,
        '#ajax' => array(
          'callback' => 'pieces_form_ajax_callback',
          'wrapper' => 'pieces-callback-wrapper',
        ),
        '#default_value' => $entity_id,
        '#entity_type' => $entity_type,
        '#bundles' => array($bundle),
        '#element_validate' => array(
          'element_validate_number',
          'pieces_entity_callback_validate'
        ),
        '#description' => t('Please enter the ID of the entity to be displayed.'),
      );

      if ($autocomplete && $entity_type && in_array($entity_type, $allowed_autocomplete)) {
        $form['entity_id']['#autocomplete_path'] = 'entity-autocomplete/bundle/' . $entity_type . '/' . $bundle;
        $form['entity_id']['#element_validate'] = array('entity_autocomplete_element_validate');
        $form['entity_id']['#title'] = t('Title');
        $form['entity_id']['#description'] = t('Please enter the title of the !entity to be displayed.', array(
          '!entity' => $entity_type,
        ));

        if (!empty($entity_id) && is_numeric($entity_id)) {
          $form['entity_id']['#default_value'] = entity_autocomplete_element_label($entity_id, $entity_type);
        }
      }

      $form['entity_view_mode'] = array(
        '#type' => 'select',
        '#title' => t('View mode'),
        '#options' => $view_modes,
        '#ajax' => array(
          'callback' => 'pieces_form_ajax_callback',
          'wrapper' => 'pieces-callback-wrapper',
        ),
        '#default_value' => $view_mode,
      );
    }
  }

  if (!empty($form_state['values']['entity_id']) && in_array($form_state['triggering_element']['#name'], array(
      'entity_type',
      'bundle',
      'entity_id',
      'entity_view_mode',
    ))
  ) {
    $form_state['values']['callback_arguments'] = array(
      'entity_type' => $entity_type,
      'bundle' => $bundle,
      'entity_id' => $entity_id,
      'entity_view_mode' => $view_mode,
    );

    if ($autocomplete) {
      $form_state['values']['callback_arguments']['entity_id'] = entity_autocomplete_get_id($entity_id);
    }
  }
}

function _pieces_entity_info() {
  $entity_info = entity_get_info();
  $entity_types = array();
  $bundles = array();
  $view_modes = array();

  foreach ($entity_info as $type => $info) {
    if ($info['fieldable'] && !empty($info['view modes']) && !empty($info['bundles'])) {
      $entity_types[$type] = $info['label'];
      $bundles[$type] = array('' => t('- Select -'));
      foreach ($info['bundles'] as $bundle_name => $bundle_info) {
        $bundles[$type][$bundle_name] = $bundle_info['label'];
      }
      foreach ($info['view modes'] as $view_mode_name => $view_mode_info) {
        $view_modes[$view_mode_name] = $view_mode_info['label'];
      }
    }
  }

  asort($entity_types);
  asort($bundles);
  asort($view_modes);

  return array(
    $entity_types,
    $bundles,
    $view_modes,
  );
}

/**
 * Validation callback.
 *
 * @see pieces_callback_form_entity().
 */
function pieces_entity_callback_validate(&$element, &$form_state) {
  if (!empty($element['#value']) && !form_get_error($element)) {
    $error = FALSE;
    $type = $element['#entity_type'];
    $id = $element['#value'];

    $entities = entity_load($type, array($id));
    if (empty($entities[$id])) {
      $error = TRUE;
    }
    else {
      $entity = $entities[$id];
      list(, , $bundle) = entity_extract_ids($type, $entity);
      if (!in_array($bundle, $element['#bundles'])) {
        $error = TRUE;
        $id = entity_label($type, $entity);
      }
    }

    if ($error) {
      form_error($element, t('The specified entity %value does not match requirements.', array('%value' => $id)));
    }
  }
}

/**
 * Pieces callback form: Views
 */
function pieces_callback_form_views(&$form, &$form_state) {
  $args = $form_state['piece']['callback_arguments'];
  $views = _pieces_get_views();

  $form['views'] = array(
    '#title' => t('Views'),
    '#type' => 'select',
    '#options' => $views,
    '#required' => TRUE,
    '#ajax' => array(
      'callback' => 'pieces_form_ajax_callback',
      'wrapper' => 'pieces-callback-wrapper',
    ),
    '#default_value' => !empty($args['name']) ? $args['name'] . ':' . $args['display'] : NULL,
  );

  if (!empty($form_state['values']['views']) && $form_state['triggering_element']['#name'] == 'views') {
    list($name, $display) = explode(':', $form_state['values']['views']);

    $form_state['values']['callback_arguments'] = array(
      'name' => $name,
      'display' => $display,
    );
  }
}

/**
 * Helper function: returns a list of views.
 *
 * @return array
 */
function _pieces_get_views() {
  $views_list = &drupal_static(__FUNCTION__, array());

  if (empty($views_list)) {
    $views = views_get_all_views();
    $views_list = array();

    foreach ($views as $view_name => $view) {
      foreach ($view->display as $display) {
        $views_list[$view_name][$view_name . ':' . $display->id] = $view->human_name . ' (' . $display->display_title . ')';
      }
    }
  }

  ksort($views_list);

  return $views_list;
}

/**
 * Pieces callback form: Function
 */
function pieces_callback_form_function(&$form, &$form_state) {
  // leave empty.
}
