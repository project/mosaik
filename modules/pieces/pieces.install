<?php

/**
 * Implements hook_schema().
 */
function pieces_schema() {
  // Schema for mosaik pieces
  $schema['pieces'] = array(
    'description' => 'The base table for mosaik pieces.',
    'fields' => array(
      'name' => array(
        'type' => 'varchar',
        'length' => 64,
        'not null' => TRUE,
        'default' => '',
      ),
      'title' => array(
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'callback' => array(
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'callback_arguments' => array(
        'type' => 'blob',
        'size' => 'big',
        'not null' => TRUE,
        'serialize' => TRUE,
        'object default' => array(),
      ),
      'access_callback' => array(
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'access_arguments' => array(
        'type' => 'blob',
        'size' => 'big',
        'not null' => TRUE,
        'serialize' => TRUE,
        'object default' => array(),
      ),
      'file' => array(
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'file_path' => array(
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      )
    ),
    'foreign keys' => array(
      'affected_mosaik' => array(
        'table' => 'mosaik',
        'columns' => array('name' => 'mosaik_name'),
      ),
    ),
    'primary key' => array('name'),
  );

  $schema['mosaik_pieces'] = array(
    'description' => 'The associative table for mosaik and pieces.',
    'fields' => array(
      'mosaik_name' => array(
        'description' => 'The {mosaik}.name.',
        'type' => 'varchar',
        'length' => 64,
        'not null' => TRUE,
        'default' => '',
      ),
      'piece_name' => array(
        'description' => 'The {pieces}.name.',
        'type' => 'varchar',
        'length' => 64,
        'not null' => TRUE,
        'default' => '',
      ),
      'weight' => array(
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
        'description' => 'Piece weight within region.',
      ),
      'region' => array(
        'type' => 'varchar',
        'length' => 64,
        'not null' => TRUE,
        'default' => '',
        'description' => 'Mosaik layout region within which the piece is set.',
      ),
    ),
    'foreign keys' => array(
      'affected_mosaik' => array(
        'table' => 'mosaik',
        'columns' => array('name' => 'mosaik_name'),
      ),
      'affected_piece' => array(
        'table' => 'pieces',
        'columns' => array('name' => 'piece_name'),
      ),
    ),
    'primary key' => array('mosaik_name', 'piece_name'),
  );

  $schema['cache_pieces'] = drupal_get_schema_unprocessed('system', 'cache');
  $schema['cache_pieces']['description'] = 'Mosaik pieces cache table';

  return $schema;
}

/**
 * Add {cache_pieces} table.
 */
function pieces_update_7001() {
  if (!db_table_exists('cache_pieces')) {
    $schema = drupal_get_schema_unprocessed('system', 'cache');
    db_create_table('cache_pieces', $schema);
  }
}
