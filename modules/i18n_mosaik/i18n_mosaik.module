<?php

/**
 * @file
 * Mosaik translation.
 */

/**
 * Implements hook_menu().
 */
function i18n_mosaik_menu() {
  $items = array();
  $include_path = drupal_get_path('module', 'i18n_mosaik') . '/includes';

  $items[MOSAIK_ADMIN_MENU . '/sets'] = array(
    'title' => 'Translation sets',
    'page callback' => 'i18n_mosaik_translation_sets_overview',
    'access arguments' => array('administer mosaik'),
    'type' => MENU_LOCAL_TASK,
    'file' => 'i18n_mosaik.admin.inc',
    'file path' => $include_path,
    'weight' => 20,
  );

  $items[MOSAIK_ADMIN_MENU . '/sets/add'] = array(
    'title' => 'Create new translation',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('i18n_mosaik_translation_form'),
    'access arguments' => array('administer mosaik'),
    'type' => MENU_LOCAL_ACTION,
    'file' => 'i18n_mosaik.admin.inc',
    'file path' => $include_path,
  );

  $items[MOSAIK_ADMIN_MENU . '/sets/edit/%i18n_mosaik_translation_set'] = array(
    'title' => 'Edit translation',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('i18n_mosaik_translation_form', 5),
    'access arguments' => array('administer mosaik'),
    'type' => MENU_CALLBACK,
    'file' => 'i18n_mosaik.admin.inc',
    'file path' => $include_path,
  );

  $items[MOSAIK_ADMIN_MENU . '/sets/delete/%i18n_mosaik_translation_set'] = array(
    'title' => 'Delete translation',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('i18n_translation_set_delete_confirm', 5),
    'access arguments' => array('administer mosaik'),
    'type' => MENU_CALLBACK,
  );

  $items['i18n/mosaik/autocomplete/language/%'] = array(
    'title' => 'Autocomplete mosaik',
    'page callback' => 'i18n_mosaik_autocomplete_language',
    'page arguments' => array(4),
    'access arguments' => array('access content'),
    'type' => MENU_CALLBACK,
    'file' => 'i18n_mosaik.pages.inc',
    'file path' => $include_path,
  );

  $items[MOSAIK_ADMIN_MENU . '/%mosaik/translate'] = array(
    'title' => 'Translate',
    'page callback' => 'i18n_mosaik_translation_page',
    'page arguments' => array('mosaik', 3),
    'access callback' => 'i18n_mosaik_translation_access',
    'access arguments' => array(3, 'administer mosaik'),
    'file' => 'i18n_mosaik.i18n.inc',
    'type' => MENU_LOCAL_TASK,
    'context' => MENU_CONTEXT_PAGE | MENU_CONTEXT_INLINE,
    'weight' => 10,
  );

  return $items;
}

/**
 * Menu access callback for Mosaik translations.
 */
function i18n_mosaik_translation_access($mosaik, $permission) {
  return (i18n_mosaik_is_translatable($mosaik) && user_access($permission));
}

/**
 * Implements hook_form_FORM_ID_alter()
 */
function i18n_mosaik_form_mosaik_page_alter(&$form, &$form_state) {
  // Check for confirmation forms
  if (isset($form_state['confirm_delete']) || isset($form_state['confirm_parents'])) {
    return;
  }

  $mosaik = $form_state['mosaik'];
  $behavior['menu'] = !empty($mosaik) ? $mosaik['menu'] : NULL;
  $behavior['menu'] = isset($form_state['values']['menu']) ? $form_state['values']['menu'] : $behavior['menu'];

  if (i18n_mosaik_is_multilingual($behavior)) {
    // Mark form so we can know later when saving the mosaik it cames from i18n.
    $form['i18n_mosaik_form'] = array(
      '#type' => 'value',
      '#value' => 1
    );

    $form['mosaik']['settings']['language'] = array(
        '#description' => t('Mosaik are multilingual. You can set a language for it.'),
      ) + i18n_element_language_select($mosaik);
  }

  $translation_form = FALSE;
  if (i18n_mosaik_is_translatable($behavior)) {
    // If the mosaik to be added will be a translation of a source mosaik,
    // set the default value of the option list to the target language and
    // create a form element for storing the translation set of the source mosaik.
    if (empty($mosaik) && isset($_GET['translation']) && isset($_GET['target']) &&
      ($source_mosaik = mosaik_load($_GET['translation'])) && ($target_language = i18n_language_object($_GET['target']))
    ) {
      $translation_form = TRUE;

      // Set context language to target language.
      i18n_language_context($target_language);

      // Add the translation set to the form so we know the new mosaik
      // needs to be added to that set.
      if (!empty($source_mosaik['i18n_tsid'])) {
        $translation_set = i18n_mosaik_translation_set_load($source_mosaik['i18n_tsid']);
      }
      else {
        // No translation set yet, build a new one with the source mosaik.
        $translation_set = i18n_translation_set_create('mosaik', $source_mosaik['name'])
          ->add_item($source_mosaik)->save();
      }

      foreach ($source_mosaik as $field => $value) {
        if (isset($form['mosaik']['settings']['mosaik'][$field])) {
          if ($field == 'name') {
            $value .= '_' . $target_language->language;
          }
          $form['mosaik']['settings']['mosaik'][$field]['#default_value'] = $value;
        }
      }

      $form['mosaik']['settings']['access']['access_type']['#default_value'] = $source_mosaik['access_type'];
      $form['mosaik']['settings']['access']['access_by_' . $source_mosaik['access_type']]['#default_value'] = $source_mosaik['access'];
      $form['mosaik']['settings']['appearence']['layout']['#default_value'] = $source_mosaik['layout'];
      $form['mosaik']['settings']['language']['#value'] = $target_language->language;
      $form['mosaik']['settings']['language']['#access'] = FALSE;
      $form['mosaik']['settings']['mosaik']['menu']['#value'] = $source_mosaik['menu'];
      $form['mosaik']['settings']['mosaik']['menu']['#access'] = FALSE;

      drupal_set_title(t('%language translation of Mosaik %title', array(
        '%language' => locale_language_name($_GET['target']),
        '%title' => $source_mosaik['title']
      )), PASS_THROUGH);
    }
    elseif (!empty($mosaik['name']) && i18n_object_langcode($mosaik)) {
      // Set context language to mosaik language.
      i18n_language_context(i18n_language_object($mosaik['language']));
      // If the current mosaik is part of a translation set,
      // remove all other languages of the option list.
      if (!empty($mosaik['i18n_tsid'])) {
        $translation_set = i18n_mosaik_translation_set_load($mosaik['i18n_tsid']);
        if ($translation_set) {
          $translations = $translation_set->get_translations();
          // If the number of translations equals 1, there's only a source translation.
          if (count($translations) > 1) {
            foreach ($translations as $langcode => $translation) {
              if ($translation['name'] !== $mosaik['name']) {
                unset($form['mosaik']['settings']['language']['#options'][$langcode]);
              }
            }
            $form['mosaik']['settings']['language']['#description'] = t('This Mosaik already belongs to a <a href="@mosaik-translation">translation set</a>. Changing language to <i>Language neutral</i> will remove it from the set.', array('@mosaik-translation' => url(MOSAIK_ADMIN_MENU . '/' . $mosaik['name'] . '/translate')));
          }
        }
      }
    }
  }

  // If we've got a translation set, add it to the form.
  if (!empty($translation_set)) {
    $form['translation_set'] = array(
      '#type' => 'value',
      '#value' => $translation_set,
    );
  }

  if (!$translation_form && user_access('translate interface') && i18n_mosaik_is_translatable($behavior)) {
    $form['mosaik']['actions']['translate'] = array(
      '#type' => 'submit',
      '#name' => 'save_translate',
      '#value' => t('Save and translate'),
      '#weight' => 5,
      '#states' => array(
        'invisible' => array(
          // Hide the button if mosaik is language neutral.
          'select[name=language]' => array('value' => LANGUAGE_NONE),
        ),
      ),
    );

    // Make sure the delete buttons shows up last.
    if (isset($form['mosaik']['actions']['delete'])) {
      $form['mosaik']['actions']['delete']['#weight'] = 10;
    }

    $form['#submit'][] = 'i18n_mosaik_form_mosaik_submit';
  }
}

/**
 * Form submit callback to redirect when using the save and translate button.
 */
function i18n_mosaik_form_mosaik_submit($form, &$form_state) {
  if ($form_state['triggering_element']['#name'] == 'save_translate') {
    // When using the edit link on the list mosaiks, a destination param is
    // added that needs to be unset to make the redirection work.
    unset($_GET['destination']);
    $form_state['redirect'] = MOSAIK_ADMIN_MENU . '/' . $form_state['values']['name'] . '/translate';
  }
}

/**
 * Load translation set. Menu loading callback.
 */
function i18n_mosaik_translation_set_load($tsid) {
  return i18n_translation_set_load($tsid, 'mosaik');
}

/**
 * Implements hook_mosaik_presave()
 */
function i18n_mosaik_mosaik_presave($mosaik, &$record) {
  if (isset($mosaik['language'])) {
    $record['language'] = $mosaik['language'];
  }
}

/**
 * Implements hook_mosaik_default_alter().
 */
function i18n_mosaik_mosaik_default_alter(&$mosaik) {
  $mosaik['language'] = LANGUAGE_NONE;
}

/**
 * Implements hook_i18n_translate_path()
 */
function i18n_mosaik_i18n_translate_path($path) {
  if ($mosaiks = mosaik_load_multiple_by_criteria(array('path' => $path))) {
    $mosaik = reset($mosaiks);
    return i18n_mosaik_translate_path($path, $mosaik);
  }
}

/**
 * Implements hook_i18n_context_language().
 */
function i18n_mosaik_i18n_context_language() {
  if ($mosaiks = mosaik_load_multiple_by_criteria(array('path' => current_path()))) {
    $mosaik = reset($mosaiks);
    if ($langcode = i18n_object_langcode($mosaik)) {
      return i18n_language_object($langcode);
    }
  }
}

/**
 * Find translations for mosaik paths.
 *
 * @param string $path
 *   Path to translate.
 * @param array $mosaik
 *   Mosaik to translate.
 *
 * @return array
 *   Array of links (each an array with href, title), indexed by language code.
 */
function i18n_mosaik_translate_path($path, $mosaik) {
  $links = array();

  if (!empty($mosaik['i18n_tsid'])) {
    $set = i18n_translation_set_load($mosaik['i18n_tsid']);
    foreach (language_list() as $langcode => $language) {
      if (!empty($set) && ($translation = $set->get_item($langcode))) {
        $links[$langcode] = array(
          'href' => $translation['path'],
          'title' => $translation['title'],
        );
      }
      else {
        $links[$langcode] = array(
          'href' => $path,
          'title' => $mosaik['title'],
        );
      }
    }
  }

  return $links;
}

/**
 * Implements hook_mosaik_insert().
 */
function i18n_mosaik_mosaik_insert($mosaik, $data) {
  i18n_mosaik_mosaik_update($mosaik, $data);
}

/**
 * Implements hook_mosaik_update().
 */
function i18n_mosaik_mosaik_update($mosaik, $data) {
  if (!empty($data['translation_set']) && i18n_mosaik_is_translatable($data)) {
    if (i18n_object_langcode($data)) {
      $translation_set = i18n_translation_set_load($data['translation_set']);
      if ($translation_set) {
        $translation_set->add_item($data)->save();
      }
    }
    elseif (!empty($data['original'])) {
      // Mosaik set to language neutral, remove it from translation set and
      // update set (delete if empty)
      $data['translation_set']
        ->remove_item($data['original'])
        ->update_delete();
    }
  }
}

/**
 * Check if a mosaik can be multilingual by its behavior.
 */
function i18n_mosaik_is_multilingual($mosaik) {
  return in_array($mosaik['menu'], array(
    MENU_NORMAL_ITEM,
    MENU_CALLBACK,
  ));
}

/**
 * Check if a mosaik can be translated by its behavior.
 */
function i18n_mosaik_is_translatable($mosaik) {
  return in_array($mosaik['menu'], array(
    MENU_NORMAL_ITEM,
    MENU_CALLBACK,
  ));
}
