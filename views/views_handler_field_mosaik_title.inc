<?php

/**
 * @file
 * Definition of views_handler_field_mosaik_title.
 */

/**
 * Field handler to properly show the mosaik title.
 *
 * @ingroup views_field_handlers
 */
class views_handler_field_mosaik_title extends views_handler_field_entity {

  function render($values) {
    if ($entity = mosaik_load($values->name)) {
      return mosaik_page_title($entity);
    }
  }

}
