<?php
/**
 * @file
 * default.tpl.php
 *
 * Default Mosaik layout.
 * It provides the following region variables:
 * - $content
 */
?>
<div id="mosaik-layout-default">
  <div id="content-region" style="width:100%; height: auto;">
    <?php print $content ?>
  </div>
</div>